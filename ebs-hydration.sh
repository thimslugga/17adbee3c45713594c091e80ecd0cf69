#!/bin/zsh -f

# EBS hydration script, it's got what EBS volumes crave!

# https://docs.aws.amazon.com/ebs/latest/userguide/ebs-initialize.html
# https://n2ws.com/blog/how-to-guides/pre-warm-ebs-volumes-on-aws
# https://aws.amazon.com/blogs/storage/addressing-i-o-latency-when-restoring-amazon-ebs-volumes-from-ebs-snapshots/
# https://www.youtube.com/watch?v=kaWzAEVZ6k8

# uname -m works too
case $(/usr/bin/arch) in
  arm64) PATH="/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin" ;;
  *) PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin" ;;
esac
export PATH

function log() { printf "[$(date)] ${@} \n" };

export HOMEBREW_NO_ENV_HINTS=1
export HOMEBREW_NO_INSTALL_CLEANUP=1
export HOMEBREW_NO_ENV_FILTERING=1

alias brewup="$(brew --prefix)/bin/brew update";
alias brewin="$(brew --prefix)/bin/brew install";

# /opt/homebrew/bin/fio
command -v fio >/dev/null 2>&1 \
  || { log "Required fio tool is not installed.."; brewup && brewin fio };

log "Hydrating the EBS volume.."
sudo /usr/bin/caffeinate -i $(brew --prefix)/bin/fio \
  --filename=/dev/r$(df -h / | grep -o 'disk[0-9]') \
  --rw=read \
  --bs=4096k \
  --iodepth=32 \
  --numjobs=1 \
  --group_reporting \
  --eta-newline=1 \
  --ioengine=posixaio \
  --direct=1 \
  --readonly \
  --name=ebs-prewarm \
  --output-format=json
  
log "done."