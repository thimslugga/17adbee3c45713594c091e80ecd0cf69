#!/usr/bin/env bash

export HOMEBREW_NO_ANALYTICS=1
export HOMEBREW_NO_GOOGLE_ANALYTICS=1
export HOMEBREW_NO_AUTO_UPDATE=1
export HOMEBREW_NO_INSECURE_REDIRECT=1
export HOMEBREW_CASK_OPTS="--appdir=~/Applications --require-sha"

if test ! $(which brew); then
    echo "Installing homebrew for macOS.. "
    
    # Install Apple Rosetta 2 on Apple Silicon based hosts
    if [[ $(arch) == 'arm64' ]]; then
      /usr/sbin/softwareupdate --install-rosetta --agree-to-license
    fi
    
    # Install Xcode CLI
    xcode-select --install;
    
    # Install homebrew
    /usr/bin/ruby -e "$(curl -sSfL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Disable homebrew analytics
$(brew --prefix)/bin/brew analytics off

# Installing from the API is now the default behaviour.
# Run the following so you can save space and time by running the following.
$(brew --prefix)/bin/brew untap homebrew/core
$(brew --prefix)/bin/brew untap homebrew/cask

# Brew update
$(brew --prefix)/bin/brew update

# Brew upgrade
#$(brew --prefix)/bin/brew upgrade
#$(brew --prefix)/bin/brew upgrade --cask

# Brew cleanup
$(brew --prefix)/bin/brew cleanup

#eval "$(/opt/homebrew/bin/brew shellenv)"

# Brew bundle dump
#$(brew --prefix)/bin/brew bundle dump
#$(brew --prefix)/bin/brew bundle dump --force

# Brew bundle install
$(brew --prefix)/bin/brew bundle
#$(brew --prefix)/bin/brew bundle install --file /path/to/Brewfile
#$(brew --prefix)/bin/brew bundle install --file ~/Brewfile

# View brew install packages and their dependencies
#$(brew --prefix)/bin/brew deps --tree --installed