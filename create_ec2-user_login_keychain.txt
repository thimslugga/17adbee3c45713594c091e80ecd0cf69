$ ssh ec2-3-26-159-255.ap-southeast-2.compute.amazonaws.com


    ┌───┬──┐   __|  __|_  )
    │ ╷╭╯╷ │   _|  (     /
    │  └╮  │  ___|\___|___|
    │ ╰─┼╯ │  Amazon EC2
    └───┴──┘  macOS Sonoma 14.2.1

Error reading managed configuration (2: No such file or directory). Proceeding with default configuration.
Error reading managed configuration (2: No such file or directory). Proceeding with default configuration.
Error reading managed configuration (2: No such file or directory). Proceeding with default configuration.
    
ec2-user@ip-172-31-34-123 ~ % mkdir -p ~/Library/Keychains

ec2-user@ip-172-31-34-123 ~ % security create-keychain ~/Library/Keychains/login.keychain
password for new keychain:
retype password for new keychain:

ec2-user@ip-172-31-34-123 ~ % security unlock-keychain ~/Library/Keychains/login.keychain
password to unlock /Users/ec2-user/Library/Keychains/login.keychain:

ec2-user@ip-172-31-34-123 ~ % ls -lA ~/Library/Keychains/
total 40
-r--r--r--  1 ec2-user  staff      0 Feb  9 21:41 .fl34AC2A0A
drwx------  3 ec2-user  staff     96 Feb  9 21:41 2E10E592-2698-5E8D-9C66-7AB9DD1296E6
-rw-r--r--  1 ec2-user  staff  20460 Feb  9 21:41 login.keychain-db
ec2-user@ip-172-31-34-123 ~ % security login-keychain -d user -s ~/Library/Keychains/login.keychain

ec2-user@ip-172-31-34-123 ~ % ls -lA ~/Library/Keychains/
total 40
-r--r--r--  1 ec2-user  staff      0 Feb  9 21:41 .fl34AC2A0A
drwx------  3 ec2-user  staff     96 Feb  9 21:41 2E10E592-2698-5E8D-9C66-7AB9DD1296E6
-rw-r--r--  1 ec2-user  staff  20460 Feb  9 21:41 login.keychain-db

ec2-user@ip-172-31-34-123 ~ % security default-keychain -d user
    "/Users/ec2-user/Library/Keychains/login.keychain-db"
ec2-user@ip-172-31-34-123 ~ % security list-keychains -d user
    "/Users/ec2-user/Library/Keychains/login.keychain-db"
ec2-user@ip-172-31-34-123 ~ % security default-keychain
    "/Users/ec2-user/Library/Keychains/login.keychain-db"
ec2-user@ip-172-31-34-123 ~ % security list-keychains
    "/Users/ec2-user/Library/Keychains/login.keychain-db"
    "/Library/Keychains/System.keychain"