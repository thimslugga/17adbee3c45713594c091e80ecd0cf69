- https://www.makemkv.com/
- https://www.makemkv.com/developers/usage.txt


```bash
makemkvcon --noscan --minlength=0 -r backup --decrypt disc:0
```

Convert iso to mkv:

```bash
makemkvcon mkv iso:./movie.iso all .
```

Convert files to mkv:

```bash
makemkvcon mkv file:/path/to/the/VIDEO_TS/ all .
```

By drive number:

```bash
# scan for drive number, usually it'll be 0 if you have only one drive
makemkvcon -r --cache=1 info disc:9999
```

```bash
# rip a disc by drive number to a path
makemkvcon mkv disc:0 all .
```