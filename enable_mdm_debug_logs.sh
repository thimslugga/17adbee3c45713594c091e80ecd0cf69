#!/bin/zsh -f

sudo log config --subsystem com.apple.ManagedClient --mode="level:debug,persist:debug"

sudo /usr/bin/defaults write /Library/Preferences/com.apple.MCXDebug debugOutput -2
sudo /usr/bin/defaults write /Library/Preferences/com.apple.MCXDebug collateLogs 1

sudo touch /var/db/MDM_EnableDebug

# Enable debug logging for Jamf MDM Enrollment
sudo /usr/bin/defaults write /Library/Preferences/com.jamfsoftware.jamf.plist global_log_level DEBUG

# View logs
#tail -n 100 /Library/Logs/ManagedClient/ManagedClient.log
#tail -n 100 /var/log/system.log

#sudo log stream --info --debug --predicate 'subsystem contains "com.apple.ManagedClient.cloudconfigurationd"'
#sudo log stream --info --debug --predicate 'processImagePath contains "mdmclient" OR processImagePath contains "storedownloadd"'