#!/bin/bash

# 1. Check whether the user has a password set as this is necessary to perform updates.
# 2. Check whether the user is a secure token holder as this is necessary to perform updates.
# 3. Check whether the user is a volume owner as this is necessary to perform updates.

function log() { echo "[$(date)] ${@}" };

# Set PATH depending on whether it is an x86_64 or Apple Silicon based EC2 Mac host.
# uname -m works too
case $(/usr/bin/arch) in
  arm64) PATH="/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin" ;;
  *) PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin" ;;
esac
#log $PATH

# Check whether FileVault2 enabled
function fileVaultCheck() {
  sudo fdesetup isactive
};

# Check for existing enrollment with MDM
function enrollmentCheck() {
  sudo /usr/bin/profiles status -type enrollment
  sudo /usr/bin/profiles -Lv
  sudo /usr/bin/profiles -P
  
  #cat /Library/Logs/ManagedClient/ManagedClient.log
  #log show --last 7d --predicate 'process == "mdmclient" OR subsystem == "com.apple.ManagedClient" OR processImagePath contains "mdmclient"'
};

function updateApfsPreboot() {
  sudo diskutil apfs updatePreboot /
};

# Flush DNS cache
function flushDnsCache() {
  log "The ${currUser} user is part of the admin group."
  sudo /usr/bin/dscacheutil -flushcache \
    && sudo /usr/bin/killall -HUP mDNSResponder
};

function listSwu() {
  /usr/sbin/softwareupdate -l --verbose --all
};

function downloadSwu() {
  /usr/sbin/softwareupdate -d "${swuLabel}"
};

# For Apple Silicon based EC2 Mac instances
function forceInstallSwuAndReboot() {
  sudo /usr/sbin/softwareupdate --install --all --restart --agree-to-license --force
};

# Re-run the following post update
function disableAutoSwu() {
  /usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticCheckEnabled -bool false
  /usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticDownload -bool false
  /usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticallyInstallMacOSUpdates -bool false
  /usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist ConfigDataInstall -bool false
  /usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist CriticalUpdateInstall -bool false
  /usr/bin/defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdate -bool false
};

# Issues?
function swuIssuesChecker() {
  # Check for errors
  /usr/bin/defaults read /Library/Preferences/com.apple.SoftwareUpdate.plist DDMPersistedErrorKey

  # After running softwareupdate --dump-state, you can view the info it dumps in /var/log/install.log
  /usr/sbin/softwareupdate --dump-state
  cat /var/log/install.log

  log show --last 10m --predicate "processImagePath Contains[c] 'softwareupdate'"
  #sudo log stream  --predicate "processImagePath Contains[c] 'softwareupdate'"
  #sudo log stream --predicate "processImagePath Contains[c] 'softwareupdate' AND eventMessage Contains[c] 'progress'"
  #sudo log collect --output /your/path
  #sudo log collect --archive system_logs.logarchive
};

currUser=$(/usr/bin/stat -f%Su /dev/console)
currUserGroups=$(/usr/bin/id -Gn ${currUserGroups})

log "Check if the user has a password set.."
sudo /usr/bin/plutil -p "/var/db/dslocal/nodes/Default/users/${currUser}.plist"
#/usr/bin/dscl . -read /Users/$USER accountPolicyData | sed '1,2d' | /usr/bin/xpath -e "/plist/dict/real[preceding-sibling::key='passwordLastSetTime'][1]/text()" 2> /dev/null
#/usr/bin/dscl . -read /Users/$USER AuthenticationAuthority

log "Check if users password on the host is correct.."
# if after inputing password returns nothing, then password is assumed correct.
#su "${currUser}" -c exit
/usr/bin/dscl /Local/Default -authonly "${currUser}"

log "Check if the user is a secure token holder.."
sysadminctl -secureTokenStatus "${currUser}"
#sudo sysadminctl -secureTokenOn ec2-user -password <password_goes_here>
#sudo sysadminctl -secureTokenOn ec2-user -password -
#sudo sysadminctl interactive -secureTokenOn ec2-user -password –

# To find out which users on the host are secure token holders
for user in $(dscl . list /Users UniqueID | awk '$2 > 499 {print $1}'); do
  printf "Checking ${user}..\n"
  sudo sysadminctl -secureTokenStatus "${user}"
done

# Make sure the user is part of the admin group
#dscl . -read /Groups/admin GroupMembership | awk '{print $2, $3, $4, $5, $6, $7, $8, $9}'
# 0 = true, 67 = false
#if groups "{currUser}" | grep -q -w admin; then 
if dseditgroup -o checkmember -m "${currUser}" admin > /dev/null; then
  log "The ${currUser} user is part of the admin group."
else
  log "The ${currUser} user is not part of the admin group."
  : sudo dscl . -append /Groups/admin GroupMembership "${currUser}"
fi

# Check to see user names and GUIDs together and to check if you have a valid disk owner.
#sudo fdesetup list -extended
if sudo fdesetup list -extended | grep "${currUser}"; then
  log "The ${currUser} user is a volume owner."
  # View the current list of delegated volume owners on Apple Silicon based Mac host
  sudo diskutil apfs listUsers /
  #sudo diskutil apfs listUsers / -plist
  # Replace GUID from previous command output to see more details about the user(s)
  dscl . -search /Users GeneratedUID "${userGUID}"
else
  log "The ${currUser} user is NOT a volume owner."
  #sudo fdesetup add -usertoadd "${currUser}"
fi

# Check for proxy and security appliances e.g. zscaler
# https://help.zscaler.com/zia/verifying-users-traffic-being-forwarded-zscaler-service
# The request received from you didn't come from a Zscaler IP therefore you are not going through the Zscaler proxy service
scutil --proxy
curl -vvv https://ip.zscaler.com | grep "The request received from you"

log "Sometimes you have to kick macOS to make it do what you want..\n"
#/usr/bin/defaults read /Library/Preferences/com.apple.commerce.plist
/usr/bin/defaults read /Library/Preferences/com.apple.SoftwareUpdate.plist
#sudo /bin/rm "/Library/Preferences/com.apple.SoftwareUpdate.plist"
sudo /bin/launchctl kickstart -k system/com.apple.softwareupdated
#/bin/launchctl kickstart -k system/com.apple.softwareupdated
