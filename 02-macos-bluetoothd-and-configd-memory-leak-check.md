# macOS memory leak checker script for bluetoothd & configd

To check the memory:

```
top -o mem
```

Create the script:

```sh
cat <<'EOF' | sudo tee /usr/local/bin/memory-leak-checker.sh
#!/bin/zsh --no-rcs

# Check processes e.g. configd, bluetoothd, etc for high memory consumption e.g. >=512MB and kill them.
#
# To manually check yourself, you can use the following: top -o mem
#
# Usage:
#
#  $ sudo /usr/local/bin/memory-leak-checker.sh
#    configd memory usage is normal: 17MB
#    Killing bluetoothd (PID: 83) due to high memory usage: 917MB

SCRIPT_NAME="$(basename "${0}")"
SCRIPT_VERSION="2024.08.20-dev"

# Memory ceiling in MB
THRESHOLD='512';

PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin"

# Helper functions
function iso8601() {
  date +"%Y-%m-%dT%H:%M:%S%z"
}

function log() {
  local priority
  priority="${1}"
  shift
  echo -e "$(iso8601) ${priority} ${@}"
}

check_and_kill_process() {
    local process_name process_path pid mem_usage
    process_name="${1}"
    process_path="${2}"

    pid=$(pgrep -f "${process_path}")
    if [[ -z "${pid}" ]]; then
        log INFO "${process_name} process not found."
        return
    fi

    mem_usage=$(ps -o rss= -p "${pid}" | awk '{print int($1/1024)}')
    if [[ "${mem_usage}" -gt "${THRESHOLD}" ]]; then
        log WARN "${process_name} with PID: ${pid}) has high memory usage (>= ${THRESHOLD}): ${mem_usage}MB. Killing."
        kill "${pid}"
    else
        log INFO "${process_name} with PID: ${pid} does not have high memory usage (< ${THRESHOLD}): ${mem_usage}MB"
    fi
}

check_and_kill_process "configd" "/usr/libexec/configd"
check_and_kill_process "bluetoothd" "/usr/sbin/bluetoothd"

exit 0

EOF

sudo chmod a+x /usr/local/bin/memory-leak-checker.sh
```

Create the LaunchDaemon plist file:

```sh
cat <<'EOF' | sudo tee /Library/LaunchDaemons/com.amzn.memoryleakchecker.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <!-- Contains a unique string that identifies your daemon to launchd. -->
    <key>Label</key>
    <string>com.amzn.memoryleakchecker</string>
    <!-- Contains the arguments [to exec()] used to launch your daemon. -->
    <key>ProgramArguments</key>
    <array>
        <string>/bin/zsh</string>
        <string>/usr/local/bin/memory-leak-checker.sh</string>
    </array>
    <!-- Run every 15 minutes -->
    <key>StartCalendarInterval</key>
    <array>
        <dict>
            <key>Minute</key>
            <integer>0</integer>
        </dict>
        <dict>
            <key>Minute</key>
            <integer>15</integer>
        </dict>
        <dict>
            <key>Minute</key>
            <integer>30</integer>
        </dict>
        <dict>
            <key>Minute</key>
            <integer>45</integer>
        </dict>
    </array>
    <!-- Low priority -->
    <key>Nice</key>
    <integer>20</integer>
    <key>LowPriorityIO</key>
    <true/>
</dict>
</plist>
EOF
```

Verify, should return okay:

```
plutil -lint /Library/LaunchDaemons/com.amzn.memoryleakchecker.plist
```

Install and enable:

```sh
sudo /bin/chmod 644 /Library/LaunchDaemons/com.amzn.memoryleakchecker.plist
sudo /usr/sbin/chown root:wheel /Library/LaunchDaemons/com.amzn.memoryleakchecker.plist
sudo launchctl load -w /Library/LaunchDaemons/com.amzn.memoryleakchecker.plist
```

Verify:

```
launchctl list | grep -E 'amazon|amzn|aws'
```

To disable and remove:

```sh
sudo launchctl unload -w /Library/LaunchDaemons/com.amzn.memoryleakcheck.plist
sudo rm -f /Library/LaunchDaemons/com.amzn.memoryleakcheck.plist
sudo rm -f /usr/local/bin/memory-leak-check.sh
```