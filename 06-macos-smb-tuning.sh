#!/bin/zsh

### SMB tuning script for macOS

#------------------------------------------------------------------------------
# https://apple.stackexchange.com/questions/309016/smb-share-deadlocks-since-high-sierra
# https://knowledgebase.45drives.com/kb/macos-samba-optimization/
# https://forums.unraid.net/bug-reports/prereleases/search-of-smb-shares-not-working-macos-client-r1105/page/2/?tab=comments#comment-16571
# https://photographylife.com/afp-vs-nfs-vs-smb-performance
# https://infohub.delltechnologies.com/en-us/l/powerscale-onefs-macos-client-performance-and-user-experience-optimization/onefs-and-macos-5/
# https://www.snbforums.com/threads/mac-os-smb-max-nvme-throughput-on-10gbe-network.84665/
# https://support.7fivefive.com/kb/latest/mac-os-smb-client-configuration
# https://wiki.samba.org/index.php/Configure_Samba_to_Work_Better_with_Mac_OS_X
#------------------------------------------------------------------------------

#sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string "${COMPUTER_NAME}"

killall cfprefsd

test ! -f /Library/Preferences && mkdir -p /Library/Preferences
test ! -f ~/Library/Preferences/nsmb.conf && touch ~/Library/Preferences/nsmb.conf
test ! -f /etc/nsmb.conf && sudo touch /etc/nsmb.conf

# /etc/nsmb.conf 
cat << '_HEREDOC_' | sudo tee -a /etc/nsmb.conf
# /etc/nsmb.conf

# parameter                             default value
#
# nbtimeout               + + -          1s             Timeout for resolving a NetBIOS name
# minauth                 + + -          NTLMv2         Minimum authentication level allowed
# streams                 + + +          yes            Use NTFS Streams if server supported
# soft                    + + +          no             Force all mounts to be soft
# notify_off              + + +          no             Turn off using notifications
# kloglevel               + - -          0              Turn on SMB kernel logging
# protocol_vers_map       + - -          7              Bitmap of SMB Versions that are enabled
# signing_required        + - -          no             Turn on SMB client signing
# signing_req_vers        + - -          6              Bitmap of SMB Versions that have signing required
# validate_neg_off        + - -          no             Turn off using validate negotiate
# max_resp_timeout        + + -          30s            Max time to wait for any response from server
# submounts_off           + + +          no             Turn off using submounts
# dir_cache_async_cnt     + + -          10             Max async queries to fill dir cache
# dir_cache_max           + + -          60s            Max time to cache for a dir
# dir_cache_min           + + -          30s            Min time to cache for a dir
# max_dirs_cached         + + -          Varies         Varies from 200-300 depending on RAM amount
# max_cached_per_dir      + + -          Varies         Varies from 2000-10000 depending on RAM amount
# netBIOS_before_DNS      + + +          no             Try NetBIOS resolution before DNS resolution
# mc_on                   + - -          yes            Turn on SMB multichannel (allow more than one channel per session)
# mc_prefer_wired         + - -          no             Prefer wired NIC's over wireless in multichannel mode - Some Wi-Fi networks advertise faster speeds than the connected wired network. 
# encrypt_cipher_map      + - -          15             Bitmap of SMB 3.1.1 encryption algorithms that are enabled
# force_sess_encrypt      + - -          no             Force session encryption for all mounts
# force_share_encrypt     + - -          no             Force share encryption for all mounts

[default]
# Protocol version is specified using binary bitmap
# 7 => 0111 => SMB 1/2/3 should be enabled
# 6 => 0110 => Only SMB v2/3 should be enabled
# 4 => 0100 => Only SMB v3 should be enabled
# 3 => 0011 => Only SMB v1/2 should be enabled
# 2 => 0010 => Only SMB v2 should be enabled
# 1 => 0001 => Only SMB v1 should be enabled
# Force SMB v3 only
protocol_vers_map=4

# No SMB1, so disable NetBIOS
# See the following for more details: 
# - https://support.apple.com/en-us/HT211927
port445=no_netbios

# Max time to wait for any response from server
#max_resp_timeout=600

# Use soft mounts by default
#soft=yes

# When you use an SMB 2 or SMB 3 connection, local caching is enabled by default. 
# You might want to turn off local caching if content on the server changes frequently, 
# or the Finder sometimes shows only a partial list of the contents of a share or folder 
# for a few seconds. (On busy or large file shares when using OS X El Capitan or macOS Sierra, 
# items might disappear from Finder while the local cache rebuilds.)
# See the following for more details: https://support.apple.com/en-us/101918
#dir_cache_max_cnt=0

# Disable directory caching
#dir_cache_off=yes

# Turn off packet signing due to macOS bugs
#
# See the following articles for more detais:
# - https://support.apple.com/en-us/HT205926
# - https://support.apple.com/en-us/HT212277
# - https://kb.promise.com/thread/disable-smb-signing-on-os-x-10-11-5-10-12-x/
signing_required=no

# Disable session signing
#
# See the following for more details: https://support.apple.com/en-us/HT204021
validate_neg_off=yes

# Turn off notifications
#
# Applying this setting can break workflows that require SMB notifications 
# for folder listings to be current. 
#
# Disabling change notifications can also lead to data corruption and other 
# issues where multiple users are accessing the same files and directories.
#notify_off=yes

# Use NTFS streams if supported. It is not necessary to force macOS to use 
# alternate data streams as it's been the default since macOS version 10.5.
streams=yes

# macOS 11.3 added SMB3 multichannel support and it is recommended to force macOS 
# to prefer wired connections as some Wi-Fi networks may advertise faster speeds 
# than the connected wired network.
#
# See the following for more details:
# - https://support.apple.com/en-us/102010
# - https://support.apple.com/en-us/HT212277
mc_on=on
mc_prefer_wired=yes

_HEREDOC_

# To speed up SMB file browsing, you can prevent macOS from reading .DS_Store files on SMB shares. 
# This makes the Finder use only basic information to immediately display each folder's contents 
# in alphanumeric order. See for more details: https://support.apple.com/en-us/HT208209.
defaults write com.apple.desktopservices DSDontWriteNetworkStores -bool true


### To enable smbd for making the macOS host into a SAMBA share server ###

sudo launchctl enable system/com.apple.smbd
sudo launchctl bootstrap system /System/Library/LaunchDaemons/com.apple.smbd.plist

#sudo launchctl stop com.apple.smbd && sudo launchctl start com.apple.smbd

# Restart the SMB service, without a stop/start
sudo launchctl kickstart -kp system/com.apple.smbd

# Stop the SMB Service
#sudo launchctl unload -w /System/Library/LaunchDaemons/com.apple.smbd.plist
# Start the SMB Service
#sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.smbd.plist

# Remove extended attributes for the shared directory
# $ xattr /path/to/shared/dir
# com.apple.FinderInfo
# com.apple.metadata:_kMDItemUserTags
# purgeable-drecs-fixed

#xattr /path/to/shared/dir
#xattr -d com.apple.metadata:_kMDItemUserTags /path/to/shared/dir
#xattr -d purgeable-drecs-fixed /path/to/shared/dir
#xattr -d com.apple.FinderInfo /path/to/shared/dir

# OR all directly
#
# xattr -c /path/to/shared/dir

# Check spotlight status for the network share, ideally it should be disabled
#mdutil -s /path/to/network/share/

# The macOS CLI utility smbutil can provide useful information about what features are 
# available and enabled on SMB-mounted file shares. The following command displays information 
# about mounted SMB shares and their supported features:
#smbutil statshares -a

# The smbutil command also has arguments for observing SMB3 multichannel operation. 
# These commands can be used to verify that SMB multichannel is functional and that 
# the appropriate NICs are involved in the multiple connections.
#smbutil multichannel
#smbutil multichannel -m /path/to/network/share