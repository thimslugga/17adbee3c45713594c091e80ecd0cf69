# macOS VMs

## Troubleshooting macOS MV Issues

### Temporary Files

The temporary directory cleanup is actually run by infrastructure inherited from BSD. You can read about it in the man page and by reading the following files.

It looks like `daily_clean_tmps_days` defaults to 3, meaning that a file gets deleted if it hasn’t been accessed in three days. Keep in mind that the specific details aren’t considered API, and can vary from release to release and platform to platform. For example, iOS has a very different system for handling temporary files.

```
/tmp
/private/var/tmp
```

```bash
man periodic.conf
```

```bash
cat /etc/defaults/periodic.conf
```

```bash
cat /etc/periodic/daily/110.clean-tmps
```

```bash
cat /System/Library/LaunchDaemons/com.apple.bsd.dirhelper.plist
grep -A6 StartCalendarInterval /System/Library/LaunchDaemons/com.apple.bsd.dirhelper.plist
```

To disable the temporary file cleanup service on macOS:

```bash
sudo launchctl unload /System/Library/LaunchDaemons/com.apple.bsd.dirhelper.plist
```

- https://forums.developer.apple.com/forums/thread/71382
- https://github.com/rustymyers/scripts/blob/master/shell/sysBuilder/10.8/LaunchDaemons/com.apple.bsd.dirhelper.plist
- https://support.circleci.com/hc/en-us/articles/16469801458203-Disable-dirhelper-Linux-Temp-File-Cleanup-Service
- https://apple.stackexchange.com/questions/22694/private-tmp-vs-private-var-tmp-vs-tmpdir
- https://lists.apple.com/archives/cocoa-dev/2009/Jan/msg01595.html

### Troubleshooting Slow Uploads to S3

To disable TSO, run the following command before any steps interacting with S3:

```bash
sudo sysctl net.inet.tcp.tso=0
#sudo sysctl -w net.inet.tcp.tso=0
```

- https://github.com/aws/aws-sdk/issues/469
- https://support.circleci.com/hc/en-us/articles/19334402064027-Troubleshooting-slow-uploads-to-S3-for-jobs-using-an-m1-macOS-resource-class

After disabling TSO, users will see better transfer speeds.

```
sudo launchctl unload /System/Library/LaunchDaemons/com.apple.bsd.dirhelper.plist
```

- https://support.circleci.com/hc/en-us/articles/16469801458203-Disable-dirhelper-Linux-Temp-File-Cleanup-Service
