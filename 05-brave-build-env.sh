#!/bin/bash

# https://github.com/brave/brave-browser
# https://github.com/brave/brave-browser/blob/master/README.md
# https://github.com/brave/brave-browser/wiki/macOS-Development-Environment
# https://chromium.googlesource.com/chromium/src/+/lkgr/docs/mac_build_instructions.md#system-requirements
# https://source.chromium.org/chromium/chromium/src/+/main:build/config/mac/mac_sdk.gni;l=43
# https://github.com/chromium/chromium/blob/main/docs/mac_build_instructions.md
# https://github.com/brave/brave-browser/wiki/Troubleshooting
# https://community.brave.com/t/problem-building-brave-on-mac/529418
# https://github.com/brock/node-reinstall
# https://gist.github.com/brock/5b1b70590e1171c4ab54

# nodejs lts
$(brew --prefix)/bin/brew install node@20
#$(brew --prefix)/bin/brew link --force --overwrite node
$(brew --prefix)/bin/brew install wget
$(brew --prefix)/bin/brew install gcc 
$(brew --prefix)/bin/brew install cmake
$(brew --prefix)/bin/brew install ninja
$(brew --prefix)/bin/brew install ccache
$(brew --prefix)/bin/brew install fswatch
$(brew --prefix)/bin/brew install gnu-time
$(brew --prefix)/bin/brew install  git git-lfs gh

cat <<'EOF' | tee -a "${HOME}/.zshrc"

export PATH="/opt/homebrew/opt/node@20/bin:$PATH"
export LDFLAGS="-L/opt/homebrew/opt/node@20/lib"
export CPPFLAGS="-I/opt/homebrew/opt/node@20/include"

# https://github.com/chromium/chromium/blob/main/docs/mac_build_instructions.md
test -d ~/depot_tools && export PATH="$PATH:$HOME/depot_tools"

EOF

node --version

# npm settings
#npm config set ~/.local/share/node_modules
npm config set progress=false
npm config set registry https://registry.npmjs.org/
#cat ~/.npmrc

# yarn
npm install --global yarn
yarn --version
yarn set version stable
yarn config set enableTelemetry false

# Build brave
mkdir src
git clone https://github.com/brave/brave-core.git src/brave
cd src/brave

# Git tuning
# https://github.blog/2022-06-29-improve-git-monorepo-performance-with-a-file-system-monitor/
# https://groups.google.com/a/chromium.org/g/chromium-dev/c/MbTkba8g_MU
git config --global core.untrackedCache true
git config --global core.fsmonitor true
git update-index --test-untracked-cache
git config core.untrackedCache true
git config core.fsmonitor true

screen

~/src/brave/vendor/depot_tools/gn args --list ~/src/out/release_arm64
~/src/brave/vendor/depot_tools/gn args ~/src/out/release_arm64

time /usr/bin/caffeinate -i npm install

npm install -g npm@10.4.0

#npm config set target_os macos
#npm config set target_arch arm64

time /usr/bin/caffeinate -i npm run init

# Building Brave
time /usr/bin/caffeinate -i npm run build release
#/usr/bin/caffeinate -i npm run build

#npm start [Release|Component|Static|Debug]

#~/src/brave/vendor/depot_tools/gn clean ~/src/out/release_arm64

#screen -d -r <pid>