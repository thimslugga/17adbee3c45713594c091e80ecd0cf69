#!/bin/bash

function get_proxy() {
    p="$1"
    enable="$(scutil --proxy | grep "${p}Enable" | cut -d: -f2)"
    host="$(scutil --proxy | grep "${p}Proxy" | cut -d: -f2 | tr -d " ")"
    port="$(scutil --proxy | grep "${p}Port" | cut -d: -f2 | tr -d " ")"
    
    if [[ "$enable" -eq 1 ]] ; then
        echo "${host}:${port}"
    fi
};

http=$(get_proxy HTTP)
https=$(get_proxy HTTPS)
ftp=$(get_proxy FTP)

[ -n "$http" ] && echo export http_proxy=\"$http\"
[ -n "$https" ] && echo export https_proxy=\"$https\"
[ -n "$ftp" ] && echo export ftp_proxy=\"$ftp\"