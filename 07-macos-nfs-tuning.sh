#!/bin/zsh

# NFS tuning script for macOS

sudo touch /etc/nfs.conf

cat <<'EOF' | sudo tee -a /etc/nfs.conf

nfs.server.bonjour = 0
nfs.server.bonjour.local_domain_only = 0

# Possible fix for mount issues
nfs.client.mount.options = vers=4.0

# -o mount option (a comma-separated string of options like e.g. option1,option2=val,option3
nfs.client.mount.options = mountport=2049

# A zero value treats the machine as a desktop for traditional NFS behavior, where hard mounts never time out.
nfs.client.is_mobile = 0

# The default is 12 seconds
nfs.client.initialdowndelay = 30

EOF

echo "done."