#!/bin/zsh -f 

cat <<'EOF' | tee privatelogs.cpp
#include <mach/mach_host.h>
#include <stdint.h>
#include <stdio.h>

const uint32_t private_data_flag = 1 << 24;

int main(int argc, char **argv) {
    if (argc == 2 && ++argv) {
        uint32_t diagnostic_flag;
        host_get_atm_diagnostic_flag(mach_host_self(), &diagnostic_flag);
        if (!strcmp(*argv, "status")) {
            puts(diagnostic_flag & private_data_flag ? "enabled" : "disabled");
            return 0;
        } else if (!strcmp(*argv, "enable")) {
            return host_set_atm_diagnostic_flag(mach_host_self(), diagnostic_flag | private_data_flag);
        } else if (!strcmp(*argv, "disable")) {
            return host_set_atm_diagnostic_flag(mach_host_self(), diagnostic_flag & ~private_data_flag);
        }
    } else {
        fprintf(stderr, "Usage: %s <status|enable|disable>\n", *argv);
    }
}
EOF

/usr/bin/g++ privatelogs.cpp -o privatelogs

./privatelogs enable
./privatelogs status