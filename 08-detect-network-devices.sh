#!/bin/bash

PLIST="/Library/LaunchDaemons/com.amzn.detectnewhw.plist";

cat <<EOF | sudo tee "${PLIST}"
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple Computer//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <!-- Contains a unique string that identifies your daemon to launchd. -->
    <key>Label</key>
    <string>com.amzn.detectnewhw</string>
    <!-- Contains the arguments [to exec()] used to launch your daemon. -->
    <key>ProgramArguments</key>
    <array>
        <string>/usr/sbin/networksetup</string>
        <string>-detectnewhardware</string>
    </array>
    <!-- Run every boot -->
    <key>RunAtLoad</key>
    <true/>
    <!-- low priority -->
    <key>Nice</key>
    <integer>20</integer>
    <key>LowPriorityIO</key>
    <true/>
</dict>
</plist>
EOF

/bin/chmod 644 "${PLIST}"
/usr/sbin/chown root:wheel "${PLIST}"