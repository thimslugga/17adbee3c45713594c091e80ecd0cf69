# macOS Performance Benchmarking

## System Information

### General System Information

```sh
hostinfo
```

```sh
/usr/sbin/system_profiler SPHardwareDataType
```

### Gather System Diagnose (sysdiagnose)

Gather system diagnostic in a gzipped tarball:

```shell
sudo /usr/bin/sysdiagnose -b -u -A sysdiagnose-$(hostname).tar.gz -f ~/
```

### System CPU Details

```shell
sysctl -n machdep.cpu.brand_string

sysctl machdep.cpu
sysctl hw.ncpu

sysctl hw.physicalcpu hw.logicalcpu
```

```shell
sysctl -a | grep brand
```

```shell
powermetrics -s cpu_power,gpu_power
```


[macmon](https://github.com/vladkens/macmon)

```shell
brew install vladkens/tap/macmon
```

TUI:

```shell
macmon
```

raw metrics:

```
macmon raw
```

[mactop](https://github.com/context-labs/mactop)

```shell
brew install mactop
```

```shell
sudo mactop
sudo mactop --interval 1000 --color green
```

[pumas](https://github.com/graelo/pumas)

```shell
brew install graelo/tap/pumas
```

```shell
sudo pumas run
```

[NeoAsitop](https://github.com/op06072/NeoAsitop)

Note: A full installation of Xcode.app is required to install and build this software. Installing just the Command Line Tools is not sufficient.

```shell
brew tap op06072/neoasitop
brew install neoasitop
```

[Asitop](https://github.com/tlkh/asitop)

```shell
brew install python3
python3 -m venv ~/.venvs/asitop --prompt asitop
source ~/.venvs/asitop/bin/activate
python3 -m pip install --upgrade pip asitop
```

```shell
sudo asitop
```

[socpowerbud](https://github.com/dehydratedpotato/socpowerbud)

https://github.com/dehydratedpotato/socpowerbud/releases

```shell
socpwrbud -a
```


[mx-power-gadget](https://www.seense.com/menubarstats/mxpg/)

```shell
brew install mx-power-gadget
```

[intel-power-gadget]()


```shell
brew install intel-power-gadget
```


- https://apple.stackexchange.com/questions/430403/graphically-display-the-information-collected-by-powermetrics

### System Memory Details

```shell
sysctl hw.memsize
```

```sh
system_profiler SPMemoryDataType
```

## System Power Metrics

```shell
sudo powermetrics -s cpu_power,gpu_power
```

## Benchmarking

```shell
brew update
brew install sysbench
brew install fio
```

### CPU Performance Benchmarking

This command tests the CPU performance by calculating prime numbers up to 20,000:

```shell
sysbench --test=cpu --cpu-max-prime=20000 run
```

### Memory Performance Benchmarking

Use sysbench for memory testing:

```shell
sysbench --test=memory run
```

### Storage Performance Benchmarking

```
IO Size (Input/Ouput size) * IOPS (Input/Output operations per second) = Throughput

MB/s and MBps (Megabytes per second) = 1,000,000 bytes per second

# There are 1000000 bits per second in 1 megabits per second
Mb/s and Mbps (Megabits per second) =

8 Bits in a Byte
KiB (Kibibytes) = 1,024 bytes
MiB (Mebibytes): 1,024 x 1,024 bytes = 1,048,576 bytes
GiB (Gibibytes): 1,024 x 1,024 x 1,024 bytes = 1,073,741,824 bytes
```

```
[Instance size]   [Baseline / Max bandwidth (Mbps)]  [Baseline / Max throughput (MB/s, 128 KiB I/O)]  [Baseline / Max IOPS (16 KiB I/O)]
mac1.metal - 14000 (Mbps) - 1750.0 (MB/s, 128 KiB I/O) 	80000 Maximum IOPS (16 KiB I/O)
mac2.metal - 10000  (Mbps) - 1250.0 (MB/s, 128 KiB I/O) 	55000 Maximum IOPS (16 KiB I/O)
mac2-m2.metal - 8000 (Mbps) - 1000.0 (MB/s, 128 KiB I/O)  55000 Maximum IOPS (16 KiB I/O)
mac2-m2pro.metal - 8000 (Mbps) - 1000.0 (MB/s, 128 KiB I/O) 55000 Maximum IOPS (16 KiB I/O)
mac2-m1ultra.metal -
mac2-m2pro.metal => Maximum throughput (MB/s, 128 KiB I/O) => 1000.0 MB/s
```

#### fio

For storage performance benchmarking, we can use fio:

- https://github.com/axboe/fio
- https://github.com/louwrentius/fio-plot
- https://github.com/louwrentius/fio-plot/tree/master/bin
- https://github.com/axboe/fio/blob/master/tools/plot/fio2gnuplot.manpage

For larger blockszie i.e. 256k, should see lower overall IOPS i.e. half what you'd get with bs=128k, so if ~8000 IOPS then expect 8000/2=~4000 IOPS.

```shell
fio --filename=./fiorandrw256k.file --direct=1 --rw=randwrite --randrepeat=0 --ioengine=posixaio --bs=256k --iodepth=8 --time_based=1 --numjobs=1 --runtime=120 --name=fio_randrw_256k --size=50G --invalidate=1 --randrepeat=0 --do_verify=0 --verify_fatal=0 --eta-newline=1 --time_based --group_reporting --clocksource=clock_gettime
```

With a blocksize i.e. bs=128k, we should expect to see both the write throughput and IOPS hit their limits together e.g. mac2-m2.metall=1000MBps.

```shell
fio --filename=./fiorandrw128k.file --direct=1 --rw=randwrite --randrepeat=0 --ioengine=posixaio --bs=128k --iodepth=8 --time_based=1 --numjobs=1 --runtime=120 --name=fio_randrw_128k --size=50G --invalidate=1 --randrepeat=0 --do_verify=0 --verify_fatal=0 --eta-newline=1 --time_based --group_reporting --clocksource=clock_gettime
```

With a smaller blocksize, we should expect to see lower write throughput but higher overall iops e.g. limit is 8000, then you should see ~8000 IOPS.

```shell
fio --filename=./fiorandrw16k.file --direct=1 --rw=randwrite --randrepeat=0 --ioengine=posixaio --bs=16k --iodepth=8 --time_based=1 --numjobs=1 --runtime=120 --name=fio_randrw_16k --size=50G --invalidate=1 --randrepeat=0 --do_verify=0 --verify_fatal=0 --eta-newline=1 --time_based --group_reporting --clocksource=clock_gettime
```

Miscellaneous tests:

```shell
fio --name=randwrite --ioengine=sync --rw=randwrite --bs=4k --numjobs=1 --size=1G --runtime=60 --group_reporting
```

#### dd

```shell
time dd if=/dev/zero of=testfile bs=1m count=1024
time dd if=testfile of=/dev/null bs=1m
rm testfile
```

#### Stibium

- https://eclecticlight.co/dintch/

```sh
curl -sL 'https://eclecticlightdotcom.files.wordpress.com/2023/06/stibium11.zip' -O
```

#### AmorphousDiskMark

- [AmorphousDiskMark i.e. CrystalDiskMark for macOS](https://apps.apple.com/us/app/amorphousdiskmark/id1168254295)
- https://www.katsurashareware.com/amorphousdiskmark/
- https://blog.greggant.com/posts/2021/03/25/amorphousdiskmark-is-crystaldiskmark-for-mac.html
- https://crystaldew.info/2016/10/30/amorphousdiskmark/
- https://www.katsurashareware.com/forum/

#### Blackmagic Disk Speed Test

- https://apps.apple.com/us/app/blackmagic-disk-speed-test/id425264550?mt=12

### Network Performance

### networkQuality

```shell
networkQuality
```

```shell
networkQuality -h
```

```shell
networkQuality -s
```

```shell
networkQuality -s -t 60
```

```shell
networkQuality -s -t 60 -i 1
```

```shell
networkQuality -s -t 60 -i 1 -f m
```

### Speedtest Files

```shell
time curl http://speedtest.ftp.otenet.gr/files/test1Mb.db -o /dev/null |& tr '\r' '\n'
```

```shell
time curl https://speed.hetzner.de/1GB.bin -o /tmp/test.1g |& tr '\r' '\n'
```

### speedtest-cli

```shell
brew install speedtest-cli
```

```shell
speedtest-cli
```

```shell
speedtest-cli --simple
```

```shell
speedtest-cli --list
```

```shell
speedtest-cli --server <server-id>
```

#### iperf3

```shell
brew update
brew install iperf3
```

```shell
iperf3 -s
```

```shell
iperf3 -c <host>
```

```shell
iperf3 -c <host> -R
```

```shell
iperf3 -c <host> -P 10
```

```shell
iperf3 -c <host> -P 10 -R
```

```shell
iperf3 -c <host> -P 10 -R -t 60
```

```shell
iperf3 -c <host> -P 10 -R -t 60 -i 1
```

```shell
iperf3 -c <host> -P 10 -R -t 60 -i 1 -f m
```

```shell
iperf3 -c <host> -P 10 -R -t 60 -i 1 -f m -w 1M
```

```shell
iperf3 -c <host> -P 10 -R -t 60 -i 1 -f m -w 1M -Z
```

#### get-network-info

- [Apple's get-network-info script](https://github.com/apple-oss-distributions/configd/blob/main/get-network-info)

## General System Monitoring

### Glances

Install via homebrew:

```sh
brew update
brew install glances
```

Usage:

```sh
glances
```

### Netdata

```sh
brew update && brew install netdata
brew services start netdata
#/opt/homebrew/opt/netdata/sbin/netdata -D
```

OR

```sh
curl -sL 'https://get.netdata.cloud/kickstart.sh' -o /tmp/netdata-kickstart.sh \
  && sh /tmp/netdata-kickstart.sh --disable-telemetry --stable-channel --non-interactive --disable-cloud

sudo launchctl kickstart -k system/com.github.netdata
```

From your local workstation:

```sh
ssh -i <keypair> -L 19999:localhost:19999 ec2-user@<host>
```

Now visit the following in your web browser: [http://localhost:19999](http://localhost:19999)

### Bottom

- https://github.com/clementtsang/bottom

Install via homebrew:

```sh
brew update
brew install bottom
```

Usage:

```sh
btm
```

### htop

Install via homebrew:

```shell
brew update
brew install htop
```

Usage:

```shell
htop
```

### CPU Monitoring

```shell
top -l 1 -n 0
top -l 1 -s 0
```

### Process Management

#### procps

```shell
ps aux

ps -ef

# userid / processid / percent cpu / percent memory / work queue / command
# -e is similar to -A (all inclusive; your processes and others), and -o is to force a format
ps -e -o "uid pid pcpu pmem wq comm"

# if you are looking for a specific uid, you can chain it using awk or grep
ps -e -o "uid pid pcpu pmem wq comm" | grep 501
```

```shell
man ps
```

#### pstree

Process Tree utility

Install via homebrew:

```shell
brew install pstree
```

```shell
pstree
pstree -u <user>        # show only processes by your user
pstree -s <string>      # show only processes with string
pstree -help            # show help
```

### Memory Usage and Management

```sh
top -o MEM
```

Swap usage:

```sh
sysctl vm.swapusage
```

#### vm_stat

```sh
vm_stat
```

```sh
paste <(vm_stat | awk 'NR>1' | grep -o ".*:") <(for i in $(vm_stat | awk 'NR>1' | tr -d '.' | awk '{print $NF}'); do perl -e "print $i/1024" | awk '{printf "%0.2f", $0}'; echo; done) | column -s: -t
```

## EC2 Mac

### ec2-macos-init

Cleanup instance specific stuff that is leftover from launch:

```sh
sudo /usr/local/bin/ec2-macos-init clean -all
```

## Apple Resources

- [https://opensource.apple.com/](https://opensource.apple.com/projects/)
- https://opensource.apple.com/releases/
- https://github.com/apple
- https://github.com/apple-open-source
- https://github.com/apple-oss-distributions
- https://github.com/Apple-FOSS-Mirror
- https://www.swift.org/
- https://www.foundationdb.org/
- https://pkl-lang.org/
- https://github.com/apple/pkl