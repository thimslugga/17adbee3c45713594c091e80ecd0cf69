#!/bin/bash

# Helper functions
function yell() { 
  echo "${0}: ${*}" >&2; 
}

function die() { 
  yell "${*}"; exit 111; 
}

# Usage: try <command>
function try() { 
  "${@}" || die "⚠️ cannot ${*}"; 
}

# Usage: retry <command>
function retry() {
  local attempt maxattempts delay
  attempt=1
  maxattempts=5
  delay=15
  
  while true; do
    "${@}" && break || {
      if [[ "${attempt}" -lt "${maxattempts}" ]]; then
        ((attempt++))
        yell "Command failed. Attempt ${attempt}/${maxattempts}:"
        sleep "${delay}";
      else
        die "The command has failed after ${attempt} attempts."
      fi
    }
  done
}

# Block until all network services have completed configuring or 
# they have timed out while they were in the process of configuring
function netwait() {
  try ipconfig waitall
}

# Sync the current date and time before proceeding with IMDS and EC2 API calls
function forcetimesync() {
  sudo /usr/bin/sntp -sS 169.254.169.123
  #ntpdate -u 169.254.169.123
}