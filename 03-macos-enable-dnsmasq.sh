#!/bin/bash

# Install dnsmasq service
$(brew --prefix)/bin/brew update
$(brew --prefix)/bin/brew install dnsmasq

# Backup the default dnsmasq.conf
cp -a /opt/homebrew/etc/dnsmasq.conf /opt/homebrew/etc/dnsmasq.conf.bak

cat <<'EOF' | sudo tee /opt/homebrew/etc/dnsmasq.conf
# /opt/homebrew/etc/dnsmasq.conf
# https://thekelleys.org.uk/dnsmasq/docs/dnsmasq-man.html
# https://thekelleys.org.uk/gitweb/?p=dnsmasq.git

## Server Configuration

# Alternative would be just 127.0.0.1 with ::1
listen-address=::1,127.0.0.1
port=53

# dnsmasq binds to the wildcard address, even if it is listening 
# on only some interfaces. It then discards requests that it 
# shouldn't reply to. This has the advantage of working even 
# when interfaces come and go and change address. 
bind-interfaces

# Uncommenting this will cause the service to fail on macOS
#interface=lo

# The userid to which dnsmasq will change after startup
#user=dnsmasq

# The group which dnsmasq will run as
#group=dnsmasq

## Name resolution options

# Don't poll /etc/resolv.conf for changes
no-poll

# Don't read /etc/resolv.conf, get upstream servers only from cli or dnsmasq conf
no-resolv

# Never forward addresses in the non-routed address spaces
bogus-priv

# Never forward plain names
domain-needed

# Reject private addresses from upstream nameservers
stop-dns-rebind

# Exempt 127.0.0.0/8 and ::1 from rebinding checks
rebind-localhost-ok

# Query servers in order
strict-order

# Set the size of dnsmasq's cache, default is 150 names
cache-size=1000

# Negative replies from upstream servers normally contain 
# time-to-live information in SOA records which dnsmasq uses 
# for caching. If the replies from upstream servers omit this 
# information, dnsmasq does not cache the reply. This option 
# gives a default value for time-to-live (in seconds) which 
# dnsmasq uses to cache negative replies even in the absence 
# of an SOA record.  
#neg-ttl=60

# Uncomment to enable validation of DNS replies and cache 
# DNSSEC data
#dnssec
#dnssec-check-unsigned
#trust-anchor=.,19036,8,2,49AAC11D7B6F6446702E54A1607371607A1A41855200FD2CE1CDDE32F24E8FB5

## Logging directives

#log-async
#log-dhcp

# Uncomment to log all queries
#log-queries

# Alternative would be /tmp/dnsmasq
log-facility=/var/log/dnsmasq.log

## Upstream servers

# AWS VPC Resolver
# https://docs.aws.amazon.com/vpc/latest/userguide/vpc-dns.html#AmazonDNS
server=169.254.169.253

# Cloudflare
#server=1.0.0.2
#server=1.1.1.2

# Additional hosts files to include
#addn-hosts=/opt/homebrew/etc/dnsmasq-blocklist

# Send queries for internal domain to internal resolver
#address=/int.example.com/10.10.10.10

# Examples of blocking TLDs or subdomains
#address=/.local/0.0.0.0
#address=/.facebook.com/0.0.0.0

# Disable iCloud Private Relay using DNS
# https://developer.apple.com/support/prepare-your-network-for-icloud-private-relay/
address=/mask.icloud.com/
address=/mask-h2.icloud.com/

# Disable Mozilla Firefox DoH using the Canary Domain
# https://support.mozilla.org/en-US/kb/canary-domain-use-application-dnsnet
address=/use-application-dns.net/

# Block enrollment with MDM
# https://support.apple.com/en-us/101555
#address=/iprofiles.apple.com/
#address=/mdmenrollment.apple.com/
#address=/deviceenrollment.apple.com/
#address=/gdmf.apple.com/
#address=/acmdm.apple.com/
#address=/albert.apple.com/

EOF

# Check the configuration syntax to make sure it's valid
dnsmasq --test

# Enable and start the service
sudo brew services start dnsmasq

# If you need to restart at all i.e. changes, etc.
#sudo brew services restart dnsmasq

# Verify
#brew services list
#ps aux | grep dnsmasq

#ifconfig
networksetup -listallhardwareports
# Set the DNS server for the active interface that was returned by the above commands
sudo networksetup -setdnsservers "Thunderbolt Ethernet Slot 0" 127.0.0.1
sudo killall -HUP mDNSResponder; sudo killall mDNSResponderHelper; sudo dscacheutil -flushcache

# Verify
scutil --dns | head
networksetup -getdnsservers "Thunderbolt Ethernet Slot 0"

dig +trace google.com

# These should return NXDOMAIN
dig mask.icloud.com
dig mask-h2.icloud.com