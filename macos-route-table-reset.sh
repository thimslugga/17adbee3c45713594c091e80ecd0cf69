#!/usr/bin/env bash

# Reset macOS route table and bounce interface

function is_root() {
  if [[ "${EUID}" -ne 0 ]]; then
    >&2 echo "This script requires super user privileges e.g. sudo.."; 
    exit 1;
  fi
}
is_root

networkInterface=$1

if [ -z ${networkInterface} ]; then 
  networkInterface="en1";
fi

# Display the current routing table
echo "********** BEFORE ****************************************"
netstat -rn
echo "**********************************************************"

# Flush the route table a few times (for good measure)
for i in {0..4}; do
  sudo route -n flush # several times
done

# Display the route table after flushing
echo "********** AFTER *****************************************"
netstat -rn
echo "**********************************************************"

# Bounce the interface
echo "Bringing interface (${networkInterface}) down..."
sudo ifconfig "${networkInterface}" down
sleep 1
echo "Bringing interface (${networkInterface}) up..."
sudo ifconfig "${networkInterface}" up
sleep 1

# Display the route table after flushing and bouncing the interface
echo "********** FINALLY ***************************************"
netstat -rn
echo "**********************************************************"