#!/usr/bin/env bash

if [[ $UID -ne 0 ]]; then 
  echo "Please run ${0} as root e.g. sudo."
  exit 1
fi

function runAsRoot() {
  # Pass in the full path to the executable as $1
  if [[ "${USER}" != "root" ]] ; then
    echo "This script requires root priviledges e.g. sudo.."
    sudo "${1}" && exit 0
  fi
}
#runAsRoot "${0}"

# Usage: runAsUser <command> <arguments>
function runAsUser() {
  local currentUser uid
  currentUser=$(stat -f%Su /dev/console)
  uid=$(id -u "${currentUser}")
  
  if [ "${currentUser}" != "loginwindow" ]; then
    launchctl asuser "${uid}" sudo -u "${currentUser}" "${@}"
  else
    echo "There is no users currently logged in to the host."
    #exit 1
  fi
}

# gid 20 is the "staff" group, which is the normal primary group for user accounts.
# gid 80 is the admin group and setting a users primary group ID to 80 will add them 
# to the admin group and hence give them admin rights on the computer.

newUser="ec2macadmin"
newUserFull="ec2macadmin"
newUserPass="stevejobs"
newUserGroups=(staff admin)

adBindCheck=$(/usr/bin/dscl localhost -list . | grep "Active Directory")

# Get curret list of users with UIDs above 1000
listCurrUsers=$(/usr/bin/dscl . list /Users UniqueID | awk '$2 > 1000 {print $1}')

# Get the next available UID
currMaxUID=$(/usr/bin/dscl . -list /Users UniqueID | awk '{print $2}' | sort -ug | tail -1)
newUserUID=$((currMaxUID+1))

# Create the new user account
# sysadminctl -addUser "${newUser}" -fullName "${newUserFull}" -password -

/usr/bin/dscl . -create "/Users/${newUser}"
/usr/bin/dscl . -create "/Users/${newUser}" UniqueID "${newUserUID}"
/usr/bin/dscl . -create "/Users/${newUser}" UserShell /bin/zsh
/usr/bin/dscl . -create "/Users/${newUser}" RealName "${newUserFull}"
/usr/bin/dscl . -create "/Users/${newUser}" PrimaryGroupID 20
/usr/bin/dscl . -create "/Users/${newUser}" NFSHomeDirectory "/Users/${newUser}"

# Create home directory for the new user
newUserHomeDir="$(/usr/bin/dscl . -read "/Users/${newUser}" NFSHomeDirectory  | awk '{print $2}')"
if [[ "$homedir" != "" ]]; then
  createhomedir -c -u "${newUser}" > /dev/null

  #mkdir -p "/Users/${newUser}"
  #/usr/sbin/chown -R "${newUser}" "${newUserHomeDir}"
  #/usr/sbin/chown -R "${newUser}":staff "${newUserHomeDir}"
fi

/usr/bin/dscl . -delete "/Users/${newUser}" AuthenticationAuthority &> /dev/null
#/usr/bin/dscl . -append "/Users/${newUser}" AuthenticationHint ''
#/usr/bin/dscl . -append "/Users/${newUser}" AvatarRepresentation ''
#/usr/bin/dscl . -append "/Users/${newUser}" inputSources ''

#/usr/bin/dscl . -delete "/Users/${newUser}" HeimdalSRPKey &> /dev/null
#/usr/bin/dscl . -delete "/Users/${newUser}" KerberosKeys &> /dev/null
#/usr/bin/dscl . -delete "/Users/${newUser}" ShadowHashData &> /dev/null
#/usr/bin/dscl . -delete "/Users/${newUser}" _writers_passwd &> /dev/null

#/usr/bin/dscl . -create "/Users/${newUser}" Password '*'
/usr/bin/dscl . -read "/Users/${newUser}" HeimdalSRPKey KerberosKeys ShadowHashData _writers_passwd Password 2>&1 | sort

#/usr/bin/dscl . -deletepl "/Users/${newUser}" accountPolicyData failedLoginCount &> /dev/null
#/usr/bin/dscl . -deletepl "/Users/${newUser}" accountPolicyData failedLoginTimestamp &> /dev/null
#/usr/bin/dscl . -deletepl "/Users/${newUser}" accountPolicyData passwordLastSetTime &> /dev/null

# To prevent a user from being granted a secure token when setting their password
#sudo /usr/bin/dscl . -append "/Users/${newUser}" AuthenticationAuthority ";DisabledTags;SecureToken"

# Set the password for the user
passwd "${newUser}"
#/usr/bin/dscl . -passwd "/Users/${newUser}" "${newUserPass}"

#/usr/bin/plutil -p "/var/db/dslocal/nodes/Default/users/${newUser}.plist"

#security set-keychain-password -o "${oldUserPass}" -p "${newUserPass}" /Users/username/Library/Keychains/login.keychain
#sudo /usr/bin/dscl . -passwd "/Users/${newUser}"
#sudo security set-keychain-password
#sudo rm -r /Users/username/Library/Keychains/* 

# Add user to any specified groups
for group in "${newUserGroups}"; do
  dseditgroup -o edit -t user -a "${newUser}" "${group}"
  #sudo /usr/bin/dscl . -merge "/Groups/${group}" GroupMembership "${newUser}"
done

#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user staff
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user admin
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user everyone
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user localaccounts
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user _appserverusr
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user _appserveradm
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user _lpadmin
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user _appstore
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user _lpoperator
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user _developer
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user com.apple.access_ssh
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user com.apple.access_screensharing
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user com.apple.access_ftp
#/usr/sbin/dseditgroup -o edit -a "${newUser}" -t user com.apple.sharepoint.group.1

# List users part of a group
/usr/bin/dscl . -read /groups/admin GroupMembership

# Verify new user is part of groups
#dsmemberutil checkmembership -U "${newUser}" -G admin
#dsmemberutil checkmembership -U "${newUser}" -G staff
#dsmemberutil checkmembership -U "${newUser}" -G wheel

# Remote Access i.e. SSH
sudo /usr/sbin/dseditgroup -o create -q com.apple.access_ssh
sudo /usr/sbin/dseditgroup -o edit -t user -a "${newUser}" com.apple.access_ssh
sudo /usr/bin/dscl  . -read /Groups/com.apple.access_ssh

# Screen Sharing
#sudo /usr/sbin/dseditgroup -o create -q com.apple.access_screensharing
#sudo /usr/sbin/dseditgroup -o edit -a "${newUser}" -t user com.apple.access_screensharing
#sudo /usr/sbin/dseditgroup -o edit -a "${localGroup}" -t group com.apple.access_screensharing
#sudo /usr/sbin/dseditgroup -o edit -a "*DomainName**DomainUserName*" -t user com.apple.access_screensharing
#sudo /usr/sbin/dseditgroup -o edit -a "*DomainName**DomainGroupName*" -t group com.apple.access_screensharing
#sudo /usr/bin/dscl  . -read /Groups/com.apple.access_screensharing
#sudo /usr/bin/dscl -f "/var/db/dslocal/nodes/Default" localonly -read /Local/Target/Groups/com.apple.access_screensharing

printf "Created user #${newUserUID}: ${newUser} (${newUserFull})\n"

#su - "${newUser}" -l
#sudo su - "${newUser}" -l