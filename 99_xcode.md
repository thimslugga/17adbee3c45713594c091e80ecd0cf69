# Xcode

## Xcode Releases

- https://developer.apple.com/documentation/xcode-release-notes/xcode-15_3-release-notes

## Xcode CLI Tools

```bash
#mkdir -p /Applications/Xcode.app
```

The process will no longer be checked for the quarantine attribute i.e. xattr:

```bash
sudo spctl developer-mode enable-terminal
```

On macOS make sure you also install the command line developer tools:

```bash
xcode-select --install
```

```bash
xcode-select --install

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew update
```

OR

```bash
macos_ver=$(sw_vers -productVersion | cut -d'.' -f 1-2);

touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;

package=$(softwareupdate -l |
  grep "\*.*Command Line *.* version ${macos_ver}" |
  head -n 1 | awk -F"*" '{print $2}' |
  sed -e 's/^ *//' |
  tr -d '\n')

softwareupdate -i "${package}" --verbose

rm /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress
```

If you didn't enable Developer Mode using Xcode you will be asked to authorize the debugger every time you use it. To enable Developer Mode and only have to authorize once per session use:

```bash
sudo /usr/sbin/DevToolsSecurity -enable
```

You might also need to add your user to the developer group:

```
sudo dscl . append /Groups/_developer GroupMembership $(whoami)
```

```bash
sudo xcode-select -s "/Applications/$(ls /Applications | grep -m 1 Xcode)"
```

## Full Xcode IDE

```
/Applications/Xcode.app/Contents/MacOS/Xcode
~/Library/Developer/Xcode/Archives
~/Library/Developer/Xcode/Archives
~/Library/Developer/Xcode/iOS DeviceSupport
~/Library/Developer/CoreSimulator/
~/Library/Developer/CoreSimulator/Devices/
~/Library/Developer/CoreSimulator/Caches/dyld/
~/Library/Logs/CoreSimulator/CoreSimulator.log
```

### iOS Simulators

- https://developer.apple.com/documentation/xcode/installing-additional-simulator-runtimes#Install-and-manage-Simulator-runtimes-from-the-command-line

A Simulator runtime is an embedded OS package that Simulator loads when running your app on a simulated device in Xcode. For example, when you test your app on a simulated iPhone running iOS 17, Simulator loads the iOS 17 Simulator runtime on the simulated device.
To minimize download size, Xcode splits the Simulator runtimes for each platform into individual installs. This allows you to install only the Simulator runtimes specific to the platform you’re developing for. Xcode requires the current versions of Simulator runtimes to build projects and to run Simulator for those platforms. To create a simulator for iPhone with a paired Apple Watch, Xcode needs the Simulator runtime for both platforms.
If you download Xcode from the Apple Developer website or the Mac App Store, you can download and install these files when you first launch Xcode, or install them later from the Xcode run destination, from Xcode Settings, or from the command line.
Manage the amount of storage that Xcode requires by choosing Xcode > Settings > Platforms to view the currently installed Simulator runtimes, and remove any that you don’t need. You can reinstall or install additional Simulator runtimes at any time.

```bash
xcode-select -s /Applications/Xcode-beta.app
xcodebuild -runFirstLaunch
xcrun simctl runtime add "~/Downloads/watchOS 9 beta Simulator Runtime.dmg"
```

To download and install all the platforms that the Xcode version you selected supports, use the -downloadAllPlatforms option on xcodebuild.

xcodebuild -downloadAllPlatforms

To download and install Simulator runtimes for a specific platform, use the -downloadPlatform option and specify the platform.

xcodebuild -downloadPlatform iOS

### xcrun

```bash
xcrun simctl delete unavailable
xcrun simctl erase all
xcrun simctl --set previews delete all
```

```bash
xcrun simctl runtime add ~/Downloads/iOS_17.2_Simulator_Runtime.dmg
```

### xcodebuild

xcodebuild is a set of commands through which you can build an Xcode project. These build tools come built-in with the Xcode IDE and help us build from a terminal. However, they're particularly useful within CI/CD pipelines, where they are typically used in wrapper scripts.

```
sudo xcodebuild -license accept
sudo xcodebuild -runFirstLaunch
sudo xcodebuild -downloadAllPlatforms
```

### xcodes

```bash
# https://github.com/RobotsAndPencils/xcodes
xcodes install --latest --experimental-unxip
```

### xcclear

- https://github.com/lajosdeme/xcclear

Run xcclear -d to diagnose your storage:

```bash
xcclear -d
```

## Xcode Tuning

## Troubleshooting Xcode Issues

- https://support.circleci.com/hc/en-us/articles/14612848580379-Hanging-Builds-when-using-the-macOS-m1-resource-class
- https://discuss.circleci.com/t/severe-performance-problems-with-xcode-15/49205/111
- https://developer.apple.com/forums/thread/735177

```bash
xcrun simctl shutdown all
```

The string is just the simulator device's ID.  To get that just follow this good guide: https://www.seanmcp.com/articles/find-an-ios-simulator-identifier/

```bash
xcrun simctl bootstatus D2AB0F53-7E2B-4533-979B-50E8392760F7 -b
xcrun simctl shutdown D2AB0F53-7E2B-4533-979B-50E8392760F7
xcrun simctl bootstatus D2AB0F53-7E2B-4533-979B-50E8392760F7 -b
```

```bash
arch -arm64 xcodebuild
```

```bash
/usr/libexec/PlistBuddy /Applications/Xcode-beta.app/Contents/Developer/Platforms/iPhoneOS.platform/Library/Developer/CoreSimulator/Profiles/Runtimes/iOS.simruntime/Contents/Resources/RuntimeRoot/Library/Wallpaper/Collections/Collections.plist -c "add order array" -c "add order: string 23EC2CF1-0188-49FC-B214-EC1AB37FE5C4"
```

```bash
while true
do
    pkill AegirPoster
    pkill InfographPoster
    pkill CollectionsPoster
    pkill ExtragalacticPoster
    pkill KaleidoscopePoster
    pkill EmojiPosterExtension
    pkill AmbientPhotoFramePosterProvider
    pkill PhotosPosterProvider
    pkill AvatarPosterExtension
    pkill GradientPosterExtension
    pkill MonogramPosterExtension
    pkill apsd
    sleep 5
done
```

## Open Source Building and Code Signing

- https://news.ycombinator.com/item?id=40041976
- https://news.ycombinator.com/item?id=32386762

### Building 

- https://github.com/tpoechtrager/osxcross
- https://github.com/ProcursusTeam/ldid
- https://manpagehub.com/ldid/ldid.1

### Code Signing

- https://gregoryszorc.com/blog/2022/08/08/achieving-a-completely-open-source-implementation-of-apple-code-signing-and-notarization/
- https://github.com/indygreg/apple-platform-rs
- https://github.com/indygreg/apple-platform-rs/tree/main/apple-codesign
- https://gregoryszorc.com/docs/apple-codesign/main/
- https://github.com/marketplace/actions/apple-code-signing
- https://github.com/indygreg/PyOxidizer/tree/faa7dfcea5d66bf50e985d7c5626971d5549ae62/apple-xar

```
# From a Git checkout
cargo run --bin rcodesign -- --help
cargo install --bin rcodesign

# Remote install.
cargo install --git https://github.com/indygreg/PyOxidizer --branch main --bin rcodesign apple-codesign 
```

- https://github.com/mitchellh/gon

- https://users.wfu.edu/cottrell/productsign/productsign_linux.html
- http://mackyle.github.io/xar/howtosign.html
- https://github.com/hogliux/bomutils

## Swift

- https://github.com/apple/swift-corelibs-foundation