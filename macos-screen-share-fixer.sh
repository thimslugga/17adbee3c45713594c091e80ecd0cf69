#!/bin/bash

# Screen Sharing Fixer

# First try to restart and disable both the ARD and Screen Sharing 
# service(s), then re-enable the screen sharing service.

# Restart and disable the Apple Remote Desktop (ARD) service
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -configure -activate -access -on -privs -all -allowaccessfor -allusers -restart -agent
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -configure -access -off

# Stop and disable the macOS Screen Sharing service
sudo launchctl unload -w /System/Library/LaunchDaemons/com.apple.screensharing.plist \
  && sudo launchctl disable system/com.apple.screensharing

# Re-enable the macOS Screen Sharing service
sudo launchctl enable system/com.apple.screensharing \
  && sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.screensharing.plist

# Now check the service e.g. listening on port 5900.
# You should see: `tcp4       0      0  *.5900`
sudo netstat -p tcp -van | grep LISTEN

# To restart just the service screen sharing services
#sudo /bin/launchctl unload /System/Library/LaunchDaemons/com.apple.screensharing.plist \
#  && sudo /bin/launchctl load -w /System/Library/LaunchDaemons/com.apple.screensharing.plist

# Check the status of the service

# If it's disabled and/or not running, then this will likely return an error e.g. 
# `Bad request. Could not find service "com.apple.screensharing" in domain for system`
sudo launchctl print system/com.apple.screensharing

# If it's enabled, this should return the following: `"com.apple.screensharing" => enabled`
# Ignore the fact that you see it in disabled services = {
sudo launchctl print-disabled system/

# View the log stream for screensharingd and related logs:
log show --last 1d --predicate 'processImagePath CONTAINS "screensharingd" AND eventMessage CONTAINS "Authentication"'
#log show --last 7d --predicate 'processImagePath CONTAINS "screensharingd" AND eventMessage CONTAINS "Authentication"'
#log stream --process screensharingd